import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import java.util.HashMap

class EiffelPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        // Add the 'eiffel' extension object
        project.extensions.create("eiffel", EiffelPluginExtension)


        // Append a closure {} to task ec
        project.task('ec') << {
            project.exec {
              commandLine 'ec', '-config', '/src/'+project.eiffel.filename+'.ecf'
            }
            project.exec {
              commandLine 'echo', 'Compiled '+ project.eiffel.filename+'.ecf'
            }

        }


        // Append a closure {} to task f_freezing ///////
        project.task('f_freezing') << {

            project.exec {
              commandLine 'finish_freezing'
              workingDir 'src/EIFGENs/'+project.eiffel.filename+'/W_code/'
            }

            project.exec {
              commandLine 'echo', 'Freezing finished for '+project.eiffel.filename
            }
        }

        project.tasks.getByName('f_freezing').dependsOn(project.tasks.getByName('ec'))

    }


}

class EiffelPluginExtension {
    def String filename
}
